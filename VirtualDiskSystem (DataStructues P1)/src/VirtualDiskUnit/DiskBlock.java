package VirtualDiskUnit;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Instances of this class represent one disk block unit with 
 * a specified capacity measured in the amount of bytes it can 
 * hold as its data.
 * @author Ivan J. Miranda Marrero
 *
 */
public class DiskBlock 
{
	private static final int DEFAULT_BLOCK_CAPACITY = 256; //Self-explanatory.
	private int blockCapacity; //How many bytes this block can hold.
	private byte[] blockByte; //The byte array that represents this block.
	
	//private long blockPointerInFile; //TODO implement to be able to update 
									//modified blocks only in file and not 
									//rewriting the entire disk for just 
									//one block modification.
	
	private boolean isDamaged; //Marker that indicates if this block is damaged.
							   //TODO re-implement some methods taking
							   //into consideration this status variable.
	
	/**
	 * Creates a disk block of size equal to 256 bytes with
	 * all bytes set to 0.
	 */
	public DiskBlock()
	{
		this(DEFAULT_BLOCK_CAPACITY);
	}
	
	/**
	 * Creates a disk block of size (number of bytes) equal to 
	 * blockCapacity with all bytes set to 0.
	 * @param blockCapacity the size of @this block in bytes.
	 */
	public DiskBlock(int blockCapacity)
	{
		this.blockCapacity = blockCapacity;
		blockByte = new byte[blockCapacity];
//		for(int i = 0; i < blockCapacity; i++)
//		{
//			blockByte[i] = (byte)0;
//		}
		isDamaged = false;
	}
	
	/**
	 * Returns a positive integer value that corresponds to the capacity 
	 * (number of bytes) of the current disk block instance.
	 * @return the size of this block in bytes.
	 */
	public int getCapacity()
	{
		return blockCapacity;
	}
	
	/**
	 * Changes the data byte at position index to the data byte specified 
	 * by parameter data in the current disk block instance. 
	 * @param index the position of the data byte that will be overwritten 
	 * with the new data byte.
	 * @param data the data byte in play.
	 */
	public void setByte(int index, byte data)
	{
		blockByte[index] = data;
	}
	
	/**
	 * Returns a copy of the byte at the position index in this block instance.
	 * @param index the position of the byte.
	 * @return the byte at index position.
	 */
	public byte getByte(int index) 
	{
		return blockByte[index];
	}
	
	/**
	 * Reads this DiskBlock's capacity of bytes (if enough data is available)
	 * into its byte array from the random access file specified starting from
	 * the current pointer in the file. If there's less data than capacity, 
	 * it will stop filling when it runs out of data to read.
	 * @param disk the random access file that represents the disk to which
	 * this block belongs.
	 * @throws IOException if an I/O Exception occurs.
	 */
	public void readBlock(RandomAccessFile disk) throws IOException
	{
		disk.read(blockByte);
	}
	
	/**
	 * Reads this DiskBlock's capacity of bytes (if enough data is available)
	 * into its byte array from the random access file specified starting from
	 * the specified pointer in the file. If there's less data than capacity, 
	 * it will stop filling when it runs out of data to read.
	 * @param disk the random access file that represents the disk to which
	 * this block belongs.
	 * @param off the position in the file from where to start reading.
	 * @throws IOException if an I/O Exception occurs.
	 */
	public void readBlock(RandomAccessFile disk, int off) throws IOException
	{

		disk.read(blockByte, off, blockCapacity);
	}

	/**
	 * Writes this DiskBlock's capacity of bytes data into the random access 
	 * file specified starting from the current pointer in the file.
	 * @param disk the random access file that represents the disk to which
	 * this block belongs.
	 * @throws IOException if an I/O Exception occurs.
	 */
	public void writeBlock(RandomAccessFile disk) throws IOException 
	{
		disk.write(blockByte);
	}
	
	/**
	 * Writes this DiskBlock's capacity of bytes data into the random access 
	 * file specified starting from the specified pointer in the file.
	 * @param disk the random access file that represents the disk to which
	 * this block belongs.
	 * @param off the position in the file from where to start writing.
	 * @throws IOException if an I/O Exception occurs.
	 */
	public void writeBlock(RandomAccessFile disk, int off) throws IOException 
	{
		disk.write(blockByte, off, blockCapacity);
	}
	
	/**
	 * Checks if this block is damaged.
	 * @return true if it is damaged.
	 */
	public boolean isDamaged()
	{
		return isDamaged;
	}
	
	/**
	 * Damages the block (isDamaged() will return true).
	 */
	public void damage()
	{
		isDamaged = true;
	}
	
	/**
	 * Repairs the block (isDamaged will return false.)
	 */
	public void repair()
	{
		isDamaged = false;
	}
	
	/**
	 * Formats the block by overwriting with zeros the 
	 * complete block.
	 */
	public void format()
	{
		for(int i = 0; i < blockCapacity; i++)
		{
			blockByte[i] = (byte)0;
		}
	}
}
