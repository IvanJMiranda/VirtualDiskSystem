package VirtualDiskTesters;
import VirtualDiskExceptions.InvalidBlockException;
import VirtualDiskExceptions.InvalidBlockNumberException;
import VirtualDiskExceptions.NonExistingDiskException;
import VirtualDiskUnit.DiskBlock;
import VirtualDiskUnit.DiskUnit;

public class DiskUnitTester1
{

	public static void main(String[] args) 
	{

		DiskUnit d;
		try {
			d = DiskUnit.mount("disk5"); //Specify here which disk to mount.
			showDiskContent(d); 
			showFileInDiskContent(d);   
			d.shutdown(); 
		} catch (NonExistingDiskException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void showFileInDiskContent(DiskUnit d) 
	{ 
		DiskBlock vdb = new DiskBlock(d.getBlockCapacity()); 
		System.out.println("\nContent of the file begining at block 1"); 
		int bn = 1; 
		while (bn != 0) 
		{ 
			try {
				d.read(bn, vdb);
			} catch (InvalidBlockNumberException | InvalidBlockException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			showDiskBlock(bn, vdb);
			bn = getNextBNFromBlock(vdb);
		}
	}

	private static void showDiskContent(DiskUnit d) 
	{ 
		System.out.println("Capacity of disk is: " + d.getDiskCapacity()); 
		System.out.println("Size of blocks in the disk is: " + d.getBlockCapacity()); 
		DiskBlock block = new DiskBlock(d.getBlockCapacity()); 
		for (int b = 0; b < d.getDiskCapacity(); b++) 
		{ 
			try {
				d.read(b, block);
			} catch (InvalidBlockNumberException | InvalidBlockException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			showDiskBlock(b, block); 
		}
	}

	private static void showDiskBlock(int b, DiskBlock block) 
	{
		System.out.print(" Block "+ b + "\t"); 
		for (int i=0; i<block.getCapacity(); i++) 
		{
			char c = (char) block.getByte(i); 
			if (Character.isLetterOrDigit(c))
			System.out.print(c); 
			else
				System.out.print('�'); 
		}
		System.out.println(); 
	}
	public static void copyNextBNToBlock(DiskBlock vdb, int value) 
	{ 
		int lastPos = vdb.getCapacity() - 1;
		for (int index = 0; index < 4; index++) 
		{ 
			vdb.setByte(lastPos - index, (byte) (value & 0x000000ff)); 
			value = value >> 8; 
		}
	}
	private static int getNextBNFromBlock(DiskBlock vdb) 
	{ 
		int bsize = vdb.getCapacity(); 
		int value = 0; 
		int lSB; 
		for (int index = 3; index >= 0; index--) 
		{ 
			value = value << 8; 
			lSB = 0x000000ff & vdb.getByte(bsize - 1 - index);
			value = value | lSB; 
		}
		return value; 
	}
}
