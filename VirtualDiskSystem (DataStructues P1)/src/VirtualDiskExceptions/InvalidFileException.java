package VirtualDiskExceptions;
@SuppressWarnings("serial")

/**
 * This exception is thrown when the file specified in the
 * process being carried is not a random access file.
 * @author Ivan J. Miranda Marrero
 *
 */
public class InvalidFileException extends Exception 
{
	/**
	 * Creates a new throwable InvalidFileException with its default message.
	 */
	public InvalidFileException()
	{
		super("Invalid file selected.");
	}
	
	/**
	 * Creates a new throwable InvalidFileException with the
	 * specified message.
	 * @param message the message to be printed when thrown.
	 */
	public InvalidFileException(String message)
	{
		super(message);
	}
}
