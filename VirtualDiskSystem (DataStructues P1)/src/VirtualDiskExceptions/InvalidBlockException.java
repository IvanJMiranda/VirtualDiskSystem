package VirtualDiskExceptions;
@SuppressWarnings("serial")

/**
 * This exception is thrown when there's a mismatch in 
 * character capacity between the specified blocks the 
 * process being carried.
 * @author Ivan J. Miranda Marrero
 *
 */
public class InvalidBlockException extends Exception 
{
	/**
	 * Creates a new throwable InvalidBlockException.
	 */
	public InvalidBlockException()
	{
		super("Mismatch between blocks.");
	}
	
	/**
	 * Creates a new throwable InvalidBlockException with the
	 * specified message.
	 * @param message the message to be printed when thrown.
	 */
	public InvalidBlockException(String message)
	{
		super(message);
	}
}
