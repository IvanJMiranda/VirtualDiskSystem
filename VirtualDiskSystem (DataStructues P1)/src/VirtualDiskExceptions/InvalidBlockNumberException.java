package VirtualDiskExceptions;
@SuppressWarnings("serial")

/**
 * This exception is thrown when the block number specified in
 * the process being carried is out of bounds.
 * @author Ivan J. Miranda Marrero
 *
 */
public class InvalidBlockNumberException extends Exception 
{
	/**
	 * Creates a new throwable InvalidBlockNumberException.
	 */
	public InvalidBlockNumberException()
	{
		super("Invalid block specified.");
	}
	
	/**
	 * Creates a new throwable InvalidBlockNumberException with the
	 * specified message.
	 * @param message the message to be printed when thrown.
	 */
	public InvalidBlockNumberException(String message)
	{
		super(message);
	}
}
