package VirtualDiskExceptions;
@SuppressWarnings("serial")

/**
 * This exception is thrown when the name specified in the
 * process of creating a new disk is that of an existing 
 * disk, that is, there's a random access file that already
 * exists with that name.
 * @author Ivan J. Miranda Marrero
 *
 */
public class ExistingDiskException extends Exception 
{
	/**
	 * Creates a new throwable NonExistingDiskException with 
	 * its default message.
	 */
	public ExistingDiskException()
	{
		super("Name specified is already in use.");
	}
	
	/**
	 * Creates a new throwable NonExistingDiskException with 
	 * the specified message.
	 * @param message the message to be printed when thrown.
	 */
	public ExistingDiskException(String message)
	{
		super(message);
	}
}
